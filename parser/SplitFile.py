#!/usr/bin/python
import sys,os
if (len(sys.argv) == 4 or len(sys.argv) == 3):
    input_str = str(sys.argv[1])
    json_count = int(sys.argv[2])

    if (len(sys.argv) == 4):rm_file = sys.argv[3]
    fo = open(input_str)
    i = 0
    file_count = 0
    chunk = ""
    while (True): # Check starting at 1
        ch = fo.read(1)
        if (ch ==''): break
        if (ch == '{'): #begin json-obj
            chunk = chunk + ch
            while (True):
                ch = fo.read(1)
                if (ch == '}'): break #end json-obj
                chunk = chunk + ch
            i = i +1
            chunk = chunk + '},'
            if ((i % json_count) == 0): #write file when enough json-objects found
                file_count = file_count+1
                f_chunk = open(str(file_count)+"_"+input_str, "wb") #filename = #_input.json
                f_chunk.write('['+chunk[:-1]+']') # drop ',' since EOF
                chunk = ""

    if (chunk != ""):
        file_count = file_count+1
        f_chunk = open(str(file_count)+"_"+input_str, "wb")
        f_chunk.write('['+chunk[:-1]+']') #drop ',' since EOF
        chunk = ""
    fo.close()
    if len(sys.argv) == 4:
        if (rm_file == 'T' or rm_file == 't'): os.remove(sys.argv[1])
    print(file_count)

elif (len(sys.argv) >4):
    print("Too many arguments supplied")
else:
    print("Error parsing JSON file")