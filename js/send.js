$(document).ready(function () {
    // Forward unique URL to parent maizegdb.website
    var URL = $(location).attr('href');
    //alert(URL.slice(49));
    var parent = $("#parent-div").html();
    console.log(parent);
    if (parent != "true") {
        window.parent.location.href = "http://staging.maizegdb.org/snpversity/send/" + URL.slice(49);
        parent = "false";
    }

//load
    $('#taxaTable').load("html/taxa_codes_modal.html").hide();
    $('#nucleotideTable').load("html/nucleotide_modal.html").hide();

//initialize loading-display for population  of table
    $(document).ajaxStart(displayLoading).ajaxStop($.unblockUI());
//Initialize table:
    rePopulate();

//Slider for zooming in & out:
    $("#slider").slider({
        value: 1,
        min: 0.4,
        max: 1.01,
        step: 0.05,
        slide: function (event, ui) {
            $("#amount").val(ui.value);
            $('#tableOfResults').css({"-moz-transform": "scale(" + ui.value + ")"});
            $('#tableOfResults').css({"-moz-transform-origin": "0% 0%"});
            $('#tableOfResults').css({"zoom": ui.value});
            //$('body,html').animate({scrollTop: 0});
        }
    });
    $("#amount").val($("#slider").slider("value"));  //update value of slider description
});

function rePopulate() {
    displayLoading();
    var file = $('#pages').find(':selected').val();
    var assembly = $('#version').html();
    if (typeof file === 'undefined') {  //For initialisation
        file = $('#first-page').html();
    }

    $.ajax({
        type: "POST",
        async: true,
        url: "get_table_body.php",
        data: 'file=' + JSON.stringify(file) + '&assembly=' + JSON.stringify(assembly), //+'&next_file='+JSON.stringify(next_file),
        beforeSend: function () {
        },
        success: function (data) {
            $('#tableOfResults tbody').html(data);
            if (data == "ERROR"){
                window.open("html/error.html", "_self");
            }
            $('body,html').animate({scrollTop: 0});
            $.unblockUI();
        }
    });
    // Show table co-ordinates?
    $('#tableOfResults td').click(function () {
        alert('My position in table is: ' + this.cellIndex + 'x' + this.parentNode.rowIndex);
    });
}

function togglePageElementVisibility(ele) {
    var obj = typeof ele == 'object'
        ? ele : document.getElementById(ele);

    if (obj.style.display == 'none')
        obj.style.display = 'block';
    else
        obj.style.display = 'none';
    return false;
}

/* Approximate zoom by padding $(function() {
 $("#slider").slider({
 value: 5,
 min: 0,
 max: 5,
 step: 1,
 slide: function (event, ui) {
 $("#amount").val(ui.value);
 $('td').css({"padding": ui.value});
 $('th').css({"padding": ui.value});
 }
 });
 $("#amount").val($("#slider").slider("value"));
 });*/

function showTaxaCodes() {
    $('#taxaTable').draggable();
    $('#taxaTable').click(function () {
    });
    if ($('#showCodeT').text() === "Show Stock Box") {
        $('#showCodeT').text("Hide Stock Box");
        $('#taxaTable').show();
        $('#taxaTable').css({"right": "0", "top": "100px"});
    }
    else {
        $('#showCodeT').text("Show Stock Box");
        $('#taxaTable').hide();
    }
}

function showNucleotideCodes() {
    $('#nucleotideTable').draggable();
    $('#nucleotideTable').click(function () {
        //onclick event
    });
    if ($('#showCodeN').text() === "Show Nucleotide Box") {
        $('#showCodeN').text("Hide Nucleotide Box");
        $('#nucleotideTable').show();
        $('#nucleotideTable').css({"left": "0", "top": "100px"});
    }
    else {
        $('#showCodeN').text("Show Nucleotide Box");
        $('#nucleotideTable').hide();
    }
}
function nextPg() {
    var current = $("#pages").val();
    var countTotal = 0;
    var index = 0;
    $("#pages option").each(function (i) {
        if ($(this).val() == current) {
            index = countTotal;
        }
        else {
            countTotal = countTotal + 1;
        }
    });
    if (index < countTotal) {
        $("#pages option").eq(index + 1).prop("selected", true).change();
    }
}
function prevPg() {
    var current = $("#pages").val();
    var countTotal = 0;
    var index = 0;
    $("#pages option").each(function (i) {
        if ($(this).val() == current) {
            index = countTotal;
        }
        else {
            countTotal = countTotal + 1;
        }
    });
    if (index > 0) {
        $("#pages option").eq(index - 1).prop("selected", true).change();
    }
}

function displayLoading() {
    //checkSubmission();
    $.blockUI({
        message: $("#loading"),
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '7px',
            '-moz-border-radius': '7px',
            opacity: .5,
            color: '#fff'
        }
    });
}